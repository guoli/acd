package com.lg.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.lg.modularization.R;

import java.util.List;

public class CustomerSpinnerAdapter extends BaseAdapter {

    private List<String> mList;
    Context mContext;

    CustomerSpinnerAdapter(Context context){
        mContext=context;
    }

    void setData(List<String> list){
        mList=list;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

TextView tv_title;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater _LayoutInflater=LayoutInflater.from(mContext);
        convertView=_LayoutInflater.inflate(R.layout.item_customer_spinner_layout, null);
        if(convertView!=null) {
            tv_title=  convertView.findViewById(R.id.tv_title);
        }
        tv_title.setText(mList.get(position));
        return convertView;
    }

}
