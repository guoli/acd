package com.lg.adapter;

import android.content.Context;
import android.util.Log;


import com.lg.bean.ChildEntity;
import com.lg.bean.GroupEntity;
import com.lg.holder.BaseViewHolder;
import com.lg.modularization.R;

import java.util.ArrayList;

/**
 * 头、尾和子项都支持多种类型的Adapter。他跟普通的{@link }基本是一样的。
 * 只需要重写{@link GroupedRecyclerViewAdapter}里的三个方法就可以实现头、尾和子项的多种类型。
 * 使用的方式跟普通的RecyclerView实现多种type是一样的。
 * {@link GroupedRecyclerViewAdapter#getHeaderViewType(int)} 返回Header的viewType
 * {@link GroupedRecyclerViewAdapter#getFooterViewType(int)}返回Footer的viewType
 * {@link GroupedRecyclerViewAdapter#getChildViewType(int, int)}返回Child的viewType
 */
public class VariousAdapter extends GroupedRecyclerViewAdapter {
String TAG = VariousAdapter.class.getSimpleName();
    private static final int TYPE_HEADER_1 = 1;
    private static final int TYPE_HEADER_2 = 2;
    private static final int TYPE_FOOTER_1 = 3;
    private static final int TYPE_FOOTER_2 = 4;
    private static final int TYPE_CHILD_1 = 5;
    private static final int TYPE_CHILD_2 = 6;

    private ArrayList<GroupEntity> mGroups;

    public VariousAdapter(Context context, ArrayList<GroupEntity> groups) {
        super(context);
        mGroups = groups;
    }

    @Override
    public int getGroupCount() {
        return mGroups == null ? 0 : mGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
//        ArrayList<ChildEntity> children = mGroups.get(groupPosition).getChildren();
//        return children == null ? 0 : children.size();
        Log.d(TAG,"isExpand(groupPosition)="+isExpand(groupPosition));
        if (!isExpand(groupPosition)) {
//            ArrayList<ChildEntity> children = mGroups.get(groupPosition).getChildren();

//            return 2;
            return 0;
        }
        ArrayList<ChildEntity> children = mGroups.get(groupPosition).getChildren();
        return children == null ? 0 : children.size();
    }

    @Override
    public boolean hasHeader(int groupPosition) {
        return true;
    }

    @Override
    public boolean hasFooter(int groupPosition) {
        return false;
    }

    @Override
    public int getHeaderLayout(int viewType) {
        return R.layout.layout_expand_group;

    }

    @Override
    public int getFooterLayout(int viewType) {
        return R.layout.layout_expand_group;

    }

    @Override
    public int getChildLayout(int viewType) {
        return R.layout.layout_expand_item;

    }

    @Override
    public void onBindHeaderViewHolder(BaseViewHolder holder, int groupPosition) {
        GroupEntity entity = mGroups.get(groupPosition);
        int viewType = getHeaderViewType(groupPosition);
        holder.setText(R.id.tv_group_text, "第一种头部：" + entity.getHeader());

    }

    @Override
    public void onBindFooterViewHolder(BaseViewHolder holder, int groupPosition) {
        GroupEntity entity = mGroups.get(groupPosition);
        int viewType = getFooterViewType(groupPosition);
        holder.setText(R.id.tv_group_text, "第一种头部：" + entity.getHeader());

    }

    @Override
    public void onBindChildViewHolder(BaseViewHolder holder, int groupPosition, int childPosition) {
        ChildEntity entity = mGroups.get(groupPosition).getChildren().get(childPosition);
        int viewType = getChildViewType(groupPosition, childPosition);
        if (viewType == TYPE_CHILD_1) {
            holder.setText(R.id.tv_item_text, "第一种子项：" + entity.getChild());
        } else if (viewType == TYPE_CHILD_2) {
            holder.setText(R.id.tv_item_text, "第二种子项：" + entity.getChild());
        }
    }

    @Override
    public int getHeaderViewType(int groupPosition) {
        if (groupPosition % 2 == 0) {
            return TYPE_HEADER_1;
        } else {
            return TYPE_HEADER_2;
        }
    }

    @Override
    public int getFooterViewType(int groupPosition) {
        if (groupPosition % 2 == 0) {
            return TYPE_FOOTER_1;
        } else {
            return TYPE_FOOTER_2;
        }
    }

    @Override
    public int getChildViewType(int groupPosition, int childPosition) {
        if (groupPosition % 2 == 0) {
            return TYPE_CHILD_1;
        } else {
            return TYPE_CHILD_2;
        }
    }


    public boolean isExpand(int groupPosition) {
        GroupEntity entity = mGroups.get(groupPosition);
        return entity.isExpand();
    }

    /**
     * 展开一个组
     *
     * @param groupPosition
     */
    public void expandGroup(int groupPosition) {
        expandGroup(groupPosition, false);
    }

    public void expandAllGroup() {
        for(int i=0;i<mGroups.size();i++){

            expandGroup(i, false);

        }
    }

    /**
     * 展开一个组
     *
     * @param groupPosition
     * @param animate
     */
    public void expandGroup(int groupPosition, boolean animate) {
        GroupEntity entity = mGroups.get(groupPosition);
        entity.setExpand(true);
        if (animate) {
            notifyChildrenInserted(groupPosition);
        } else {
            notifyDataChanged();
        }
    }

    /**
     * 收起一个组
     *
     * @param groupPosition
     */
    public void collapseGroup(int groupPosition) {
        collapseGroup(groupPosition, false);
    }

    /**
     * 收起一个组
     *
     * @param groupPosition
     * @param animate
     */
    public void collapseGroup(int groupPosition, boolean animate) {
        GroupEntity entity = mGroups.get(groupPosition);
        entity.setExpand(false);
        if (animate) {
            notifyChildrenRemoved(groupPosition);
        } else {
            notifyDataChanged();
        }
    }
}
