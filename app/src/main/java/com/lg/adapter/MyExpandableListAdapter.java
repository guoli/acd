package com.lg.adapter;

import android.content.Context;
import android.transition.Visibility;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lg.bean.Category;
import com.lg.bean.GroupItem;
import com.lg.modularization.R;

import java.util.HashMap;
import java.util.List;

public class MyExpandableListAdapter  extends BaseExpandableListAdapter {
    String TAG=MyExpandableListAdapter.class.getSimpleName();
    private Context mContext;
    private ExpandableListView expandableListView;



    private List<GroupItem> groupList;
    private List<List<Category>> childList;
    private LayoutInflater inflater;
    private HashMap<Integer, Boolean> maps = new HashMap<Integer, Boolean>();
    private OnGroupExpandListener OnGroupExpandListener;
    private OnGroupClickListener onGroupClickListener;
    private OnChildItemClickListener onChildClickListener;



    private boolean expandStateAtPosition = false;
    public boolean getExpandStateAtPosition(int groupPosition) {
        // 获得当前位置的展开状态
        expandStateAtPosition = maps.get(groupPosition).booleanValue();
        return expandStateAtPosition;
    }

    public void setExpandStateAtPosition(int groupPosition,
                                         boolean expandStateAtPosition) {
        this.expandStateAtPosition = expandStateAtPosition;
        maps.put(groupPosition, expandStateAtPosition);
    }

    public MyExpandableListAdapter.OnGroupExpandListener getOnGroupExpandListener() {
        return OnGroupExpandListener;
    }

    public void setOnGroupExpandListener(MyExpandableListAdapter.OnGroupExpandListener onGroupExpandListener) {
        OnGroupExpandListener = onGroupExpandListener;
    }

    public OnGroupClickListener getOnGroupClickListener() {
        return onGroupClickListener;
    }

    public void setOnGroupClickListener(OnGroupClickListener onGroupClickListener) {
        this.onGroupClickListener = onGroupClickListener;
    }

    public OnChildItemClickListener getOnChildClickListener() {
        return onChildClickListener;
    }

    public void setOnChildClickListener(OnChildItemClickListener onChildClickListener) {
        this.onChildClickListener = onChildClickListener;
    }

    public MyExpandableListAdapter(Context context,
                                   ExpandableListView expandableListView, List<GroupItem> groupList,
                                   List<List<Category>> childList) {
        super();
        this.mContext = context;
        this.expandableListView = expandableListView;
        this.groupList = groupList;
        this.childList = childList;

        inflater = LayoutInflater.from(context);

        // 初始化列表展开状态
        for (int i = 0; i < groupList.size(); i++) {
            maps.put(i, false);
        }
    }

    public List<List<Category>> getChildList() {
        return childList;
    }

    public void setChildList(List<List<Category>> childList) {
        this.childList = childList;
    }

    @Override
    public int getGroupCount() {
        return groupList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childList.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groupList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childList.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        final GroupViewHolder groupViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_expand_group, parent,
                    false);
            groupViewHolder = new GroupViewHolder(convertView);
            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }

         GroupItem groupItem = groupList.get(groupPosition);
        groupViewHolder.itemGroupIcon.setBackgroundResource(groupItem
                .getDrawableId());
        groupViewHolder.itemGroupText.setText(groupItem.getText());
        // 如果该组没有子项，则不显示箭头
        if (childList.get(groupPosition).size() == 0) {
            groupViewHolder.itemArrow.setVisibility(View.GONE);
            groupViewHolder.itemGroupLayout
                    .setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            onGroupClickListener.onGroupClick(groupPosition);
                        }
                    });
        } else {
            groupViewHolder.itemArrow.setVisibility(View.VISIBLE);
//            groupViewHolder.itemGroupLayout
//                    .setOnClickListener(new View.OnClickListener() {
//
//                        @Override
//                        public void onClick(View v) {
//                            //OnGroupExpandListener.onGroupExpand(groupPosition);
//                        }
//                    });
            final View finalConvertView = convertView;
            groupViewHolder.layout_showall_or_recommend.setOnClickListener(new View.OnClickListener(){

                @Override
                public void onClick(View v) {
                    if(groupViewHolder.tv_show_label.getText()!=null&&"show all".equals(groupViewHolder.tv_show_label.getText()+"")){
                        isRecommed=true;
                        recommed_groudPosition =groupPosition;
                        groupViewHolder.tv_show_label.setText("show recommended");
                        OnGroupExpandListener.onGroupExpand(groupPosition,true);
//                            for(int i =0;i<getChildrenCount(groupPosition);i++){
//                                ChildViewHolder childViewHolder= (ChildViewHolder) getChildView(groupPosition,i,false, null, (ViewGroup) finalConvertView).getTag();
//                                if(childViewHolder.iv_group_icon.getVisibility()==View.GONE){
//                                    childViewHolder.layout_expand_mitem.setVisibility(View.GONE);
//                                }
//                            }



                    }else{
                        isRecommed=false;

                        groupViewHolder.tv_show_label.setText("show all");

                        OnGroupExpandListener.onGroupExpand(groupPosition,false);

                    }
                }
            });
        }



        convertView.setClickable(false);
        return convertView;
    }

 public final static int STAR_RECOMMED =2;
     boolean isRecommed=false;
     int recommed_groudPosition;

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildViewHolder childViewHolder;

            Log.d(TAG,"convertView="+convertView);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_expand_item,
                    parent, false);
            childViewHolder = new ChildViewHolder(convertView);
            convertView.setTag(childViewHolder);
        } else {
            childViewHolder = (ChildViewHolder) convertView.getTag();
        }
        Log.d(TAG,"childPosition="+childPosition);
        Log.d(TAG,"groupPosition="+groupPosition);

        if(childPosition<STAR_RECOMMED){
            childViewHolder.iv_child_icon.setVisibility(View.VISIBLE);

        }else {
            childViewHolder.iv_child_icon.setVisibility(View.GONE);
        }



        if(groupPosition<childList.size()&&childPosition<childList.size()){
            String content = childList.get(groupPosition).get(childPosition)
                    .getTitle();

            // 设置内容
            childViewHolder.itemChildText.setText(content);

            // 设置文本点击事件
            childViewHolder.itemChildText.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    onChildClickListener.onChildItemClick(groupPosition,
                            childPosition);
                }
            });


        }

//        Log.d(TAG,"isRecommed="+isRecommed+"");
//        if(groupPosition==recommed_groudPosition){
//            Log.d(TAG,"recommed_groudPosition="+groupPosition+"");
//
//            if(isRecommed){
//
//                if(childViewHolder.iv_child_icon.getVisibility()==View.GONE){
//                    childViewHolder.layout_expand_mitem.setVisibility(View.GONE);
//                    notifyDataSetChanged();
//                }
//
//            }else {
//                childViewHolder.layout_expand_mitem.setVisibility(View.VISIBLE);
//                notifyDataSetChanged();
//
//            }
//        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    private static class GroupViewHolder {
        RelativeLayout itemGroupLayout;
        ImageView itemGroupIcon;
        TextView itemGroupText;
        ImageView itemArrow;
        View itemDivider;
        LinearLayout layout_showall_or_recommend;
        TextView tv_show_label;

        public GroupViewHolder(View convertView) {
            itemGroupLayout = (RelativeLayout) convertView
                    .findViewById(R.id.group_layout);
            itemGroupIcon = (ImageView) convertView
                    .findViewById(R.id.iv_group_icon);
            itemGroupText = (TextView) convertView
                    .findViewById(R.id.tv_group_text);
            itemArrow = (ImageView) convertView.findViewById(R.id.iv_expand);
            itemDivider = (View) convertView
                    .findViewById(R.id.item_group_devider);
            layout_showall_or_recommend=convertView.findViewById(R.id.layout_showall_or_recommend);
            tv_show_label=convertView.findViewById(R.id.tv_show_label);


        }

    }

    public static class ChildViewHolder {
        TextView itemChildText;
        public ImageView iv_child_icon;
        TextView tv_picked;
        public RelativeLayout layout_expand_mitem;
        public ChildViewHolder(View convertView) {
            itemChildText =  convertView
                    .findViewById(R.id.tv_item_text);
            iv_child_icon = convertView.findViewById(R.id.iv_child_icon);
            tv_picked= convertView.findViewById(R.id.tv_picked);
            layout_expand_mitem= convertView.findViewById(R.id.layout_expand_mitem);
        }
    }


    public interface OnGroupExpandListener {
        void onGroupExpand(int position,boolean isRecommed);
    }

    public interface OnGroupClickListener {
        void onGroupClick(int position);
    }

    public interface OnChildItemClickListener {
        void onChildItemClick(int groupPosition, int childPosition);
    }
}
