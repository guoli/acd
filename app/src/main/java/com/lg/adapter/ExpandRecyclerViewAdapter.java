package com.lg.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.lg.bean.Category;
import com.lg.bean.GroupItem;
import com.lg.modularization.R;

import java.util.ArrayList;
import java.util.List;

public class ExpandRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context mContext;
    public static final int GROUP_ITEM = 1;
    public static final int CHILD_ITEM = 2;
    private List<GroupItem> groupList;
    private List<List<Category>> childList;


    public ExpandRecyclerViewAdapter(Context context,List<GroupItem> groupList){
        mContext=context;
        this.groupList = groupList;
        //this.childList = childList;
    }

    @NonNull
    @Override
    public  RecyclerView.ViewHolder  onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        mContext=viewGroup.getContext();
        RecyclerView.ViewHolder holder = null;

        if(GROUP_ITEM==viewType){
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_expand_group, viewGroup, false);
            holder = new GroupViewHolder(view);
        }else {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_expand_item, viewGroup, false);
            holder = new ChildViewHolder(view);

        }

        return holder;
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof  GroupViewHolder){
            ((GroupViewHolder)holder).itemGroupText.setText(groupList.get(position).getText());



        }else {
            ((ChildViewHolder)holder).itemChildText.setText(groupList.get(position).getText());

        }

    }

    @Override
    public int getItemCount() {
        return groupList.size();
    }

    @Override
    public int getItemViewType(int position) {

        int mViewType = GROUP_ITEM;

        if(groupList.get(position).getChildId()!=null){
            mViewType=CHILD_ITEM;
        }else {
            mViewType = GROUP_ITEM;
        }

        return mViewType;
    }

    private static class GroupViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout itemGroupLayout;
        ImageView itemGroupIcon;
        TextView itemGroupText;
        ImageView itemArrow;
        View itemDivider;
        LinearLayout layout_showall_or_recommend;
        TextView tv_show_label;

        public GroupViewHolder(View convertView) {
            super(convertView);
            itemGroupLayout = (RelativeLayout) convertView
                    .findViewById(R.id.group_layout);
            itemGroupIcon = (ImageView) convertView
                    .findViewById(R.id.iv_group_icon);
            itemGroupText = (TextView) convertView
                    .findViewById(R.id.tv_group_text);
            itemArrow = (ImageView) convertView.findViewById(R.id.iv_expand);
            itemDivider = (View) convertView
                    .findViewById(R.id.item_group_devider);
            layout_showall_or_recommend=convertView.findViewById(R.id.layout_showall_or_recommend);
            tv_show_label=convertView.findViewById(R.id.tv_show_label);


        }

    }

    public static class ChildViewHolder extends RecyclerView.ViewHolder {
        TextView itemChildText;
        public ImageView iv_child_icon;
        TextView tv_picked;
        public RelativeLayout layout_expand_mitem;
        public ChildViewHolder(View convertView) {
            super(convertView);
            itemChildText =  convertView
                    .findViewById(R.id.tv_item_text);
            iv_child_icon = convertView.findViewById(R.id.iv_child_icon);
            tv_picked= convertView.findViewById(R.id.tv_picked);
            layout_expand_mitem= convertView.findViewById(R.id.layout_expand_mitem);
        }
    }
}
