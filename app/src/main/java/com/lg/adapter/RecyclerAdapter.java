package com.lg.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.lg.modularization.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.CustomHodler> {
    Context mContext;


    public RecyclerAdapter (Context context){
        mContext=context;
    }

    @NonNull
    @Override
    public CustomHodler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        mContext=viewGroup.getContext();
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_recyclerview_layout, viewGroup, false);
        return new CustomHodler(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomHodler customHodler, int i) {
        CustomerSpinnerAdapter  customerSpinnerAdapter = new CustomerSpinnerAdapter(mContext);

        List<String> mlist = new ArrayList<String>();
        mlist.add("12312");
        mlist.add("12312");
        mlist.add("12312");
        mlist.add("12312");
        mlist.add("12312");

        customerSpinnerAdapter.setData(mlist);
        customHodler.mSpinner.setAdapter(customerSpinnerAdapter);
       // customHodler.mSpinner.getAdapter().notify();
       // customHodler.tv_title.setText("customHodler");
    }

    @Override
    public int getItemCount() {
        return 100;
    }

    public static class CustomHodler extends RecyclerView.ViewHolder{

        Spinner mSpinner;
        TextView tv_title;

        public CustomHodler(@NonNull View itemView) {
            super(itemView);
            mSpinner = itemView.findViewById(R.id.custom_spinner);
            tv_title= itemView.findViewById(R.id.tv_title);
        }
    }
}
