package com.lg.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.lg.modularization.BuildConfig;
import com.lg.modularization.R;
import com.lg.modularization.common.base.BaseActivity;
import com.lg.modularization.common.logs.LogUtils;
import com.lg.activity.ui.MyGroupListActivity;

public class MainActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LogUtils.d();

    }

    public void toMember_Module(View view) {
        String url = BuildConfig.baseUrl;
        Toast.makeText(this," url"+url,Toast.LENGTH_SHORT).show();
        //集成化
        if (BuildConfig.isRelease) {
            Toast.makeText(this,"release can jump Member",Toast.LENGTH_SHORT).show();
            Class Member_Activity = null;
            try {
                Member_Activity = Class.forName("com.lg.modularization.member.Member_Activity");
                Intent it = new Intent(this,Member_Activity);
                startActivity(it);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(this,"Exception = "+e.getMessage(),Toast.LENGTH_SHORT).show();

            }


        } else {
            Toast.makeText(this,"debug has not Member ",Toast.LENGTH_SHORT).show();

        }
    }

    public void toPerson_Module(View view) {
       // startActivity(new Intent(this, MyPopupWindow.class));
        //startActivity(new Intent(this, MyGroupListActivity.class));
        startActivity(new Intent(this, MyFistTemplateActivity.class));



    }
}
