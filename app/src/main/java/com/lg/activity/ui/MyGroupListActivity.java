package com.lg.activity.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lg.adapter.GroupedRecyclerViewAdapter;
import com.lg.adapter.VariousAdapter;
import com.lg.bean.GroupModel;
import com.lg.holder.BaseViewHolder;
import com.lg.modularization.R;
import com.lg.modularization.common.base.BaseActivity;

public class MyGroupListActivity extends BaseActivity {
    String TAG = MyGroupListActivity.class.getSimpleName();
    RecyclerView rv_list;
    Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_grouplist);
        rv_list = findViewById(R.id.rv_list);
        rv_list.setLayoutManager(new LinearLayoutManager(this));
        VariousAdapter variousAdapter = new VariousAdapter(this, GroupModel.getGroups(10, 5));
        variousAdapter.expandAllGroup();
        variousAdapter.setOnHeaderClickListener(new GroupedRecyclerViewAdapter.OnHeaderClickListener() {
            @Override
            public void onHeaderClick(GroupedRecyclerViewAdapter adapter, BaseViewHolder holder,
                                      int groupPosition) {

                VariousAdapter mAdapter = (VariousAdapter) adapter;

                if (mAdapter.isExpand(groupPosition)) {
                    mAdapter.collapseGroup(groupPosition);
                } else {
                    mAdapter.expandGroup(groupPosition);
                }

//                Toast.makeText(mContext, "组头：groupPosition = " + groupPosition,
//                        Toast.LENGTH_LONG).show();
            }
        });
        rv_list.setAdapter(variousAdapter);
    }
}
