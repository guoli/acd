package com.lg.activity.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.lg.adapter.RecyclerAdapter;
import com.lg.modularization.R;
import com.lg.modularization.common.base.BaseActivity;

public class RecyclerViewSpinnerActivity extends BaseActivity {

    RecyclerView rv_spinner;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_spinner);
        rv_spinner= findViewById(R.id.rv_spinner);
        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        rv_spinner.setAdapter(recyclerAdapter);
        rv_spinner.setLayoutManager(layoutManager);
    }
}
