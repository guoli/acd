package com.lg.activity.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ExpandableListView;

import com.lg.adapter.MyExpandableListAdapter;
import com.lg.bean.Category;
import com.lg.bean.GroupItem;
import com.lg.modularization.R;
import com.lg.modularization.common.base.BaseActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyExpandableListViewActivity extends BaseActivity implements MyExpandableListAdapter.OnGroupClickListener, MyExpandableListAdapter.OnGroupExpandListener
        , MyExpandableListAdapter.OnChildItemClickListener{
String TAG = MyExpandableListViewActivity.class.getSimpleName();

    ExpandableListView expandable_list;
    private MyExpandableListAdapter expandAdapter;
    private List<GroupItem> groupList;
    private List<List<Category>> childList;
    private List<List<Category>>  recommedChildList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expandablelist);
        expandable_list = findViewById(R.id.expandable_list);
        // 设置展开列表数据
        setExpandableListView();


    }



    private void setExpandableListView() {
        try {
            // 这里分别模拟组项和子项数据
            groupList = new ArrayList<GroupItem>();
            childList = new ArrayList<List<Category>>();
            recommedChildList = new ArrayList<List<Category>>();


            // 从本地获取目录
            String str = getString(R.string.categories);
            JSONObject jsonObject = new JSONObject(str);
            JSONObject categoryObj = jsonObject.optJSONObject("categories");
            // 左边侧边栏
            JSONArray jsonArray = categoryObj.optJSONArray("left");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONArray groupArray = jsonArray.getJSONArray(i);
                GroupItem groupItem = new GroupItem();


                JSONObject groupObj = groupArray.optJSONObject(0);
                groupItem.setGroupId(groupObj.optString("cat_id")); // 设置组别的分类id
                groupItem.setText(groupObj.optString("title"));
                groupList.add(groupItem);
                List<Category> categories = new ArrayList<Category>();
                if (groupArray.length() > 0) { // 如果多于一项分类
                    for (int index = 1; index < groupArray.length(); index++) {
                        JSONObject itemObj = groupArray.optJSONObject(index);
                        Category categorie = new Category();
                        categorie.setTitle(itemObj.optString("title"));
                        categorie.setCat_id(itemObj.optString("cat_id"));
                        if(index <= MyExpandableListAdapter.STAR_RECOMMED){
                            categorie.setRecommed(true);
                        }else{
                            categorie.setRecommed(false);

                        }
                        categories.add(categorie);
                    }
                }

                childList.add(categories);
            }

            // 实例化适配器

            expandAdapter = new MyExpandableListAdapter(this, expandable_list, groupList, childList);
            expandable_list.setAdapter(expandAdapter);


            // 设置ExpandableListView的相关事件监听
            // 子项选中、子项被点击、组项展开、组项被点击
            // expandableListView.setOnItemSelectedListener(itemSelectedListener);
           // expandable_list.setOnChildClickListener(mOnChildClickListener);
//            expandableListView.setOnGroupExpandListener(mOnGroupExpandListener);
            // expandableListView.setOnGroupClickListener(mOnGroupClickListener);
            expandAdapter.setOnGroupClickListener(this);
            expandAdapter.setOnGroupExpandListener(this);
            expandAdapter.setOnChildClickListener(this);
            for(int i = 0; i < expandAdapter.getGroupCount(); i++){

                expandable_list.expandGroup(i);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onGroupExpand(int groupPosition,boolean isRecommed) {
        Log.d(TAG,"onGroupExpand");
        // 组项被展开时会回调这个方法
        if(isRecommed){
            recommedChildList=childList;
            for( int i =0;i<childList.get(groupPosition).size();i++){
                Category category = childList.get(groupPosition).get(i);
               if(!category.isRecommed()){
                   recommedChildList.get(groupPosition).remove(category);

               }
            }
            expandAdapter = new MyExpandableListAdapter(this, expandable_list, groupList, recommedChildList);
            //expandAdapter.notifyDataSetChanged();
//            expandable_list.removeAllViews();
            expandable_list.setAdapter(expandAdapter);
        }else{
            expandAdapter = new MyExpandableListAdapter(this, expandable_list, groupList, childList);
           // expandable_list.removeAllViews();
            expandable_list.setAdapter(expandAdapter);
            //expandAdapter.notifyDataSetChanged();

        }


//        expandable_list.expandGroup(groupPosition, true);
//           expandAdapter.setExpandStateAtPosition(groupPosition, true);
//        if (expandAdapter.getExpandStateAtPosition(groupPosition)) {
//            // 收起
//            expandable_list.collapseGroup(groupPosition);
//            expandAdapter.setExpandStateAtPosition(groupPosition, false);
//        } else {
//            expandable_list.expandGroup(groupPosition, true);
//            expandAdapter.setExpandStateAtPosition(groupPosition, true);
//        }
        //expandAdapter.
       // expandable_list.expandGroup(position, true);

          // expandAdapter.setExpandStateAtPosition(position, true);
        //int romevedNum= expandAdapter.getChildList().get(groupPosition).size()-MyExpandableListAdapter.STAR_RECOMMED;
            //expandable_list.expandGroup(groupPosition, true);

//        if(isRecommed){
//            for( int i =0;i<childList.get(groupPosition).size();i++){
//                Category category = childList.get(groupPosition).get(i);
//                if(!category.isRecommed()){
//                    expandable_list.getExpandableListAdapter().getChild(groupPosition,0);
//                   // expandAdapter.getChildList().get(groupPosition).remove(category);
//                }
//            }
//
//
//
//        }else {
//            for( int i =0;i<childList.get(groupPosition).size();i++){
//                Category category = childList.get(groupPosition).get(i);
//                if(!category.isRecommed()){
//                  //  expandAdapter.getChildList().get(groupPosition).add(i,category);
//                }
//            }
//        }
                  //  expandable_list.expandGroup(groupPosition, true);

       // expandAdapter.setExpandStateAtPosition(groupPosition, true);


        // expandAdapter.setExpandStateAtPosition(groupPosition, true);

//        for(int i =0;i<expandAdapter.getChildrenCount(groupPosition);i++){
//            //MyExpandableListAdapter.ChildViewHolder childViewHolder= (MyExpandableListAdapter.ChildViewHolder) expandable_list.getChildAt(i).getTag();
//            expandable_list.getAdapter().getItem(0);
//            MyExpandableListAdapter.ChildViewHolder childViewHolder= (MyExpandableListAdapter.ChildViewHolder) expandAdapter.getChildView(groupPosition,i,false, expandable_list,expandable_list).getTag();
//            if(childViewHolder.iv_child_icon.getVisibility()== View.GONE){
//                childViewHolder.layout_expand_mitem.setVisibility(View.GONE);
//            }
//        }
    }

    @Override
    public void onGroupClick(int position) {
        Log.d(TAG,"onGroupClick");

    }

    @Override
    public void onChildItemClick(int groupPosition, int childPosition) {
        Log.d(TAG,"onChildItemClick");

    }
}
