package com.lg.activity.ui;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.lg.modularization.R;
import com.lg.modularization.common.base.BaseActivity;

public class MyPopupWindow extends BaseActivity {

    Context mContext;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypopupwindow);

        mContext=this;
    }

    public void onClick_showPopup(View view) {
        initPopWindow(view);
    }

    private void initPopWindow(View v) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_popupwindow, null, false);
        Button btn_nofiler = (Button) view.findViewById(R.id.btn_nofiler);
        Button btn_del_in = (Button) view.findViewById(R.id.btn_del_in);
        Button btn_del = (Button) view.findViewById(R.id.btn_del);
        final ImageView iv_check_del = view.findViewById(R.id.iv_check_del);
        //1.构造一个PopupWindow，参数依次是加载的View，宽高
        final PopupWindow popWindow = new PopupWindow(view,
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

       // popWindow.setAnimationStyle(R.anim.anim_pop);  //设置加载动画

        //这些为了点击非PopupWindow区域，PopupWindow会消失的，如果没有下面的
        //代码的话，你会发现，当你把PopupWindow显示出来了，无论你按多少次后退键
        //PopupWindow并不会关闭，而且退不出程序，加上下述代码可以解决这个问题
        popWindow.setTouchable(true);
        popWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
                // 这里如果返回true的话，touch事件将被拦截
                // 拦截后 PopupWindow的onTouchEvent不被调用，这样点击外部区域无法dismiss
            }
        });
        popWindow.setBackgroundDrawable(new ColorDrawable(0x00000000));    //要为popWindow设置一个背景才有效


        //设置popupWindow显示的位置，参数依次是参照View，x轴的偏移量，y轴的偏移量
        popWindow.showAsDropDown(v, -10, 0);

        //Drawable img_on, img_off;
        //Resources res = getResources();
        //img_off = res.getDrawable(R.drawable.btn_strip_mark_off);
        ////调用setCompoundDrawables时，必须调用Drawable.setBounds()方法,否则图片不显示
        //img_off.setBounds(0, 0, img_off.getMinimumWidth(), img_off.getMinimumHeight());
        //btn.setCompoundDrawables(img_off, null, null, null); //设置左图标


        //设置popupWindow里的按钮的事件
        btn_nofiler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(mContext, "你点击了嘻嘻~1", Toast.LENGTH_SHORT).show();
                popWindow.dismiss();

            }
        });
        btn_del_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "你点击了呵呵~2", Toast.LENGTH_SHORT).show();
                popWindow.dismiss();
            }
        });
        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iv_check_del.setVisibility(View.VISIBLE);
                Toast.makeText(mContext, "你点击了呵呵~3", Toast.LENGTH_SHORT).show();
                popWindow.dismiss();
            }
        });
    }
}
