package com.lg.activity.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ExpandableListAdapter;

import com.lg.adapter.ExpandRecyclerViewAdapter;
import com.lg.adapter.MyExpandableListAdapter;
import com.lg.adapter.RecyclerAdapter;
import com.lg.bean.Category;
import com.lg.bean.GroupItem;
import com.lg.modularization.R;
import com.lg.modularization.common.base.BaseActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ExpandRecyclerViewActivity extends BaseActivity {
    String TAG= ExpandableListAdapter.class.getSimpleName();

    RecyclerView rv_contentlist;
    private List<GroupItem> groupList;
    private List<List<Category>> childList;
    private List<List<Category>>  recommedChildList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_child_layout);
        rv_contentlist= findViewById(R.id.rv_contentlist);

        init();

    }
    void init(){
        try{
            groupList = new ArrayList<GroupItem>();
            childList = new ArrayList<List<Category>>();
            recommedChildList = new ArrayList<List<Category>>();


            // 从本地获取目录
            String str = getString(R.string.categories);
            JSONObject jsonObject = new JSONObject(str);
            JSONObject categoryObj = jsonObject.optJSONObject("categories");
            // 左边侧边栏
            JSONArray jsonArray = categoryObj.optJSONArray("left");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONArray groupArray = jsonArray.getJSONArray(i);
                GroupItem groupItem = new GroupItem();


                JSONObject groupObj = groupArray.optJSONObject(0);
                groupItem.setGroupId(groupObj.optString("cat_id")); // 设置组别的分类id
                groupItem.setText(groupObj.optString("title"));
                groupList.add(groupItem);
                List<Category> categories = new ArrayList<Category>();
                if (groupArray.length() > 0) { // 如果多于一项分类
                    for (int index = 1; index < groupArray.length(); index++) {
                        JSONObject itemObj = groupArray.optJSONObject(index);
                        Category categorie = new Category();
                        categorie.setTitle(itemObj.optString("title"));
                        categorie.setCat_id(itemObj.optString("cat_id"));
                        if(index <= MyExpandableListAdapter.STAR_RECOMMED){
                            categorie.setRecommed(true);
                        }else{
                            categorie.setRecommed(false);

                        }
                        categories.add(categorie);
                    }
                }

                childList.add(categories);
            }

            ExpandRecyclerViewAdapter recyclerAdapter = new ExpandRecyclerViewAdapter(this,groupList);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(this);

            rv_contentlist.setAdapter(recyclerAdapter);
            rv_contentlist.setLayoutManager(layoutManager);
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG,"error="+e.getMessage());
        }

    }
}
