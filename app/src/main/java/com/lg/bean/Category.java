package com.lg.bean;

public class Category {
	private String title;
	private String cat_id;
	private boolean recommed;



	public boolean isRecommed() {
		return recommed;
	}

	public void setRecommed(boolean recommed) {
		this.recommed = recommed;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCat_id() {
		return cat_id;
	}

	public void setCat_id(String cat_id) {
		this.cat_id = cat_id;
	}



}
