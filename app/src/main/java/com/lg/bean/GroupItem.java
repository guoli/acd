package com.lg.bean;

import java.util.List;

public class GroupItem {
	private int drawableId;
	private String text;
	private String groupId;

	//
	private String childId;
	private boolean recommed;
	private int pickstatus;


	//List<Category> categoriesList;

	public int getDrawableId() {
		return drawableId;
	}
	public void setDrawableId(int drawable) {
		this.drawableId = drawable;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getChildId() {
		return childId;
	}

	public void setChildId(String childId) {
		this.childId = childId;
	}

	public boolean isRecommed() {
		return recommed;
	}

	public void setRecommed(boolean recommed) {
		this.recommed = recommed;
	}

	public int getPickstatus() {
		return pickstatus;
	}

	public void setPickstatus(int pickstatus) {
		this.pickstatus = pickstatus;
	}

	//	public List<Category> getCategoriesList() {
//		return categoriesList;
//	}
//
//	public void setCategoriesList(List<Category> categoriesList) {
//		this.categoriesList = categoriesList;
//	}
}
