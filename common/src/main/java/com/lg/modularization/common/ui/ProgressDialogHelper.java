/*
 * @(#)ProgressDialogHelper.java        2014 2:09:02 PM
 *
 * Copyright (c) 2004-2014 i-Sprint Technologies, Inc.
 * address
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of 
 * i-Sprint Technologies, Inc. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with i-Sprint.
 */
package com.lg.modularization.common.ui;

import android.app.Activity;
import android.content.ContextWrapper;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ProgressDialog;
import android.content.Context;

import com.lg.modularization.common.R;


public class ProgressDialogHelper {
    private static ProgressDialogHelper progressDialogHelper = null;

    private ProgressDialog progressDialog = null;

    private Timer timer = new Timer();

    private TimerTask task;

    public static ProgressDialogHelper getInstance() {
        if (progressDialogHelper == null) {
            progressDialogHelper = new ProgressDialogHelper();
        }
        return progressDialogHelper;

    }

    public void showDialog(final Context context, String str) {
        try {
            if (progressDialog != null) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            progressDialog = new ProgressDialog(context);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(str);
            if (!progressDialog.isShowing()) {
                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                    progressDialog.show();
                    task = new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                if (!((Activity) context).isFinishing()
                                    && !((Activity) context).isDestroyed()) {
                                    if (progressDialog.isShowing()) {
                                        cancleDialog();
                                    }
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }catch (Error e){
                                e.printStackTrace();
                            }
                        }
                    };
                    timer = new Timer();
                    timer.schedule(task, 25000);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }catch (Error e){
            e.printStackTrace();
        }
    }

    public void showDialog(Context context, String title, String str) {
        try {
            if (progressDialog != null) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            progressDialog = new ProgressDialog(context);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(false);
            progressDialog.setMessage(str);
            progressDialog.setTitle(title);
            if (!progressDialog.isShowing()) {
                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                    progressDialog.show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }catch (Error e){
            e.printStackTrace();
        }
    }

    public void showDialog(Context context, String str, boolean cancelable) {
        try {
            if (progressDialog != null) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            progressDialog = new ProgressDialog(context);
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(cancelable);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage(str);
            if (!progressDialog.isShowing()) {
                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                    progressDialog.show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }catch (Error e){
            e.printStackTrace();
        }
    }

    public void showDialog(Context context) {
        try {
            if (progressDialog != null) {
                try {
                    progressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            progressDialog = new ProgressDialog(context);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setTitle(R.string.loading);
            progressDialog.setMessage(context.getResources().getString(R.string.loadingmsg));
            progressDialog.setCanceledOnTouchOutside(false);
            if (!progressDialog.isShowing()) {
                if (!((Activity) context).isFinishing() && !((Activity) context).isDestroyed()) {
                    progressDialog.show();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }catch (Error e){
            e.printStackTrace();
        }

    }

    public void cancleDialog() {
        try {
            if (progressDialog != null) {
                Context context = ((ContextWrapper) progressDialog.getContext()).getBaseContext();
                //判断是所在Activity是否已经销毁
                if (context instanceof Activity) {
                    if (!((Activity) context).isFinishing()
                        && !((Activity) context).isDestroyed()) {
                        progressDialog.dismiss();
                    }
                }
                else {
                    progressDialog.dismiss();
                }
            }
            if (timer != null) {
                timer.cancel();
                timer.purge();
                timer = null;
            }
            progressDialog = null;
        }catch (Exception e){
            e.printStackTrace();
        }catch (Error e){
            e.printStackTrace();
        }

    }

}
