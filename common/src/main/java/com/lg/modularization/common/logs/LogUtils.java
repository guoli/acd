package com.lg.modularization.common.logs;

import android.util.Log;

public final class LogUtils {
 
	private static boolean sIsLogEnabled = true;// open log
 
	private static String TAG = LogUtils.class.getSimpleName();
 
	private static final String TAG_CONTENT_PRINT = "[%s.%s|line=%d]";
 
	private static StackTraceElement getCurrentStackTraceElement() {
		return Thread.currentThread().getStackTrace()[4];
 
	}
 
	//print LOG
	public static void trace() {
		if (sIsLogEnabled) {
			Log.d(TAG,
					getContent(getCurrentStackTraceElement()));
		}
	}

	//get LOG
	private static String getContent(StackTraceElement trace) {
		return String.format(TAG_CONTENT_PRINT, trace.getClassName(), trace.getMethodName(),
				trace.getLineNumber());
	}
	//defualt log
	public static void traceStack() {
		if (sIsLogEnabled) {
			traceStack(TAG, Log.ERROR);
		}
	}

	// Log
	public static void traceStack(String tag, int priority) {

		if (sIsLogEnabled) {
			StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
			Log.println(priority, tag, stackTrace[4].toString());
			StringBuilder str = new StringBuilder();
			String prevClass = null;
			for (int i = 5; i < stackTrace.length; i++) {
				String className = stackTrace[i].getFileName();
				int idx = className.indexOf(".java");
				if (idx >= 0) {
					className = className.substring(0, idx);
				}
				if (prevClass == null || !prevClass.equals(className)) {

					str.append(className.substring(0, idx));

				}
				prevClass = className;
				str.append("[").append(stackTrace[i].getMethodName())
						.append("|line=").append(stackTrace[i].getLineNumber())
						.append("] ");
			}
			Log.println(priority, tag, str.toString());
		}
	}
	//指定TAG和指定内容的方法
	public static void d(String tag, String msg) {
		if (sIsLogEnabled) {
			Log.d(tag, getContent(getCurrentStackTraceElement())+" "+msg);
		}
	}
	//默认TAG和制定内容的方法
	public static void d(String msg) {
		if (sIsLogEnabled) {
			Log.d(TAG, getContent(getCurrentStackTraceElement())+" "+msg);
		}
	}

	//默认TAG和制定内容的方法
	public static void d() {
		if (sIsLogEnabled) {
			Log.d(TAG, getContent(getCurrentStackTraceElement()));
		}
	}
	//下面的定义和上面方法相同，可以定义不同等级的Debugger
	public static void i(String tag,String msg){
		if (sIsLogEnabled) {
			Log.i(TAG, getContent(getCurrentStackTraceElement())+" "+msg);
		}
		
	}
	public static void w(String tag,String msg){
		if (sIsLogEnabled) {
			Log.w(TAG, getContent(getCurrentStackTraceElement())+" "+msg);
		}
		
	}
	public static void e(String tag,String msg){
		if (sIsLogEnabled) {
			Log.e(tag, getContent(getCurrentStackTraceElement())+" "+msg);
		}
	}
	public static void e(String msg){
		if (sIsLogEnabled) {
			Log.e(TAG, getContent(getCurrentStackTraceElement())+" "+msg);
		}
	}
}