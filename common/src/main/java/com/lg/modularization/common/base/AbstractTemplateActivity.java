package com.lg.modularization.common.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.lg.modularization.common.R;
import com.lg.modularization.common.logs.LogUtils;

public abstract  class AbstractTemplateActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template_layout);
        LogUtils.d();
    }


    public abstract void setHead();

    public abstract void setHead2();

    public abstract void setbody();

    public abstract void setfoot();

    public void onFinish(){
        finish();
    }


}
