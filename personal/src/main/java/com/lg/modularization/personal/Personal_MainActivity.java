package com.lg.modularization.personal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lg.modularization.common.base.BaseActivity;

public class Personal_MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.persion_activity_main);
    }
}
